# Nvimcontainer

My personal [neovim](https://github.com/neovim/neovim) configuration packaged into a container.
Notable features of the configuration is that its setup is completely automatic. After installing
all packages the first start of neovim can be headless, all setup processes like installing
treesitter parsers and langauge servers get triggered and neovim will quit once everything has
finished. After that the editor is ready to be used.

## Usage

To give the editor access to the files you are working on it is necessary to mount it into the
`/root/project` directory in the container. Additionally the container has to be started with an
interactive tty shell.

> Ex. `podman run -it -v $PWD:/root/project registry.gitlab.com/jgero/nvimcontainer`

Some language servers store additional data or some kind of cache in directories outside of the
project directory. By re-using containers or mounting additional volumes long startup times or even
errors can be avoided.

