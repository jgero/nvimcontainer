local M = {}

local lspconfig = require('lspconfig')
local lspconfig_utils = require('jgero.config.lsp')

require('nvim-lsp-installer').setup()

local function trim_version(string_with_version)
	local i = string.find(string_with_version, '@')
	if i then
		return string.sub(string_with_version, 1, i - 1)
	end
	return string_with_version
end

M.setup = function(wanted_servers)
	for _, server in pairs(wanted_servers) do
		local config_opts = {
			capabilities = lspconfig_utils.capabilities,
			on_attach = lspconfig_utils.on_attach,
		}
		server = trim_version(server)

		if server == 'sumneko_lua' then
			config_opts.settings = {
				Lua = {
					diagnostics = {
						globals = { 'vim' },
					},
					runtime = {
						version = 'LuaJIT',
						path = vim.split(package.path, ';'),
					},
					workspace = {
						library = {
							[vim.fn.expand('$VIMRUNTIME/lua')] = true,
							[vim.fn.expand('$VIMRUNTIME/lua/vim/lsp')] = true,
						},
					},
				},
			}
		end

		-- if server == 'svelte' or server == 'yamlls' or server == 'bashls' then
		-- 	config_opts.before_init = function (params) params.processId = vim.NIL end
		-- 	config_opts.root_dir = lspconfig.util.root_pattern("package.json", ".git")
		-- end

		lspconfig[server].setup(config_opts)
	end
end

return M
